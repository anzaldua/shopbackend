<?php 

	class ControladorAdministradores{

		/*===============================================
		=            Ingreso Administradores            =
		===============================================*/
		
		public function ctrIngresoAdministrador(){

			if(isset($_POST["ingEmail"])){

				if(preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["ingEmail"]) &&
				   preg_match('/^[a-zA-Z0-9]+$/', $_POST["ingPassword"])){

					//$encriptar = crypt($_POST["ingPassword"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');

					$tabla = "administradores";
					$item = "email";
					$valor = $_POST["ingEmail"];

					$respuesta = ModeloAdministradores::mdlMostrarAdministradores($tabla, $item, $valor);

					if($respuesta["email"] == $_POST["ingEmail"] && $respuesta["password"] == $_POST["ingPassword"]){

						$_SESSION["validarSesionAdmin"] = "ok";
						$_SESSION["idAdmin"] = $respuesta["id"];
						$_SESSION["nombreAdmin"] = $respuesta["nombre"];
						$_SESSION["fotoAdmin"] = $respuesta["foto"];
						$_SESSION["emailAdmin"] = $respuesta["email"];
						$_SESSION["passwordAdmin"] = $respuesta["password"];
						$_SESSION["perfilAdmin"] = $respuesta["perfil"];

						echo '<script>

							  	window.location = "inicio";

							  </script>';

					}else{

						echo '<div class="alert alert-danger" style="margin-top:15px;">Error al Ingresar, vuelva a intentarlo</div>';

					}
				}

			}

		}
		
		/*=====  End of Ingreso Administradores  ======*/
		

	}