<?php 

	class ControladorVentas{

		/*============================================
		=            Mostrar Total Ventas            =
		============================================*/
		
		public function ctrMostrarTotalVentas(){

			$tabla = "compras";

			$respuesta = ModeloVentas::mdlMostrarTotalVentas($tabla);

			return $respuesta;

		}
		
		/*=====  End of Mostrar Total Ventas  ======*/

		/*======================================
		=            Mostrar Ventas            =
		======================================*/

		public function ctrMostrarVentas(){

			$tabla = "compras";

			$respuesta = ModeloVentas::mdlMostrarVentas($tabla);

			return $respuesta;

		}
		
		
		
		/*=====  End of Mostrar Ventas  ======*/
		
				

	}