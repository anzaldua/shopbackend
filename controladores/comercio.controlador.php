<?php 

	class ControladorComercio{

		/*=============================================
		=            Seleccionar Plantilla            =
		=============================================*/
		
		static public function ctrSeleccionarPlantilla(){

			$tabla = "plantilla";

			$respuesta = ModeloComercio::mdlSeleccionarPlantilla($tabla);

			return $respuesta;

		}

		/*===============================================
		=            Actualizar Logo o Icono            =
		===============================================*/
		
		static public function ctrActualizarLogoIcono($item, $valor){

			$tabla = "plantilla";
			$id = 1;

			$plantilla = ModeloComercio::mdlSeleccionarPlantilla($tabla);

			/* Cambiando Logotipo o Icono*/
			
			if(isset($valor["tmp_name"])){

				list($ancho, $alto) = getimagesize($valor["tmp_name"]);

					/* Cambiando Logotipo */

					if($item == "logo"){

						unlink("../".$plantilla["logo"]);

						$nuevoAncho = 500;
						$nuevoAlto = 100;

						$destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

						if($valor["type"] == "image/jpeg"){

							$ruta = "../vistas/img/plantilla/logo.jpg";

							$origen = imagecreatefromjpeg($valor["tmp_name"]);

							imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

							imagejpeg($destino, $ruta);

						}

						if($valor["type"] == "image/png"){

							$ruta = "../vistas/img/plantilla/logo.png";

							$origen = imagecreatefrompng($valor["tmp_name"]);

							imagealphablending($destino, FALSE);

							imagesavealpha($destino, TRUE);

							imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

							imagepng($destino, $ruta);

						}

					}

				/* Cambiando Icono */
				
				if($item == "icono"){



				}

				$valorNuevo = substr($ruta, 3);
			}

			$respuesta = ModeloComercio::mdlActualizarLogoIcono($tabla, $id, $item, $valorNuevo);

			return $respuesta;

		}
		
		/*=====  End of Actualizar Logo o Icono  ======*/

	}