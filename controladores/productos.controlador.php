<?php 

	class ControladorProductos{

		/*============================================
		=            Mostrar Total Productos            =
		============================================*/
		
		static public function ctrMostrarTotalProductos($orden){

			$tabla = "productos";

			$respuesta = ModeloProductos::mdlMostrarTotalProductos($tabla, $orden);

			return $respuesta;

		}
		
		/*=====  End of Mostrar Total Productos  ======*/

		/*===========================================
		=            Mostrar Suma Ventas            =
		===========================================*/
		
		public function ctrMostrarSumaVentas(){

			$tabla = "productos";

			$respuesta = ModeloProductos::mdlMostrarSumaVentas($tabla);

			return $respuesta;

		}
		
		/*=====  End of Mostrar Suma Ventas  ======*/
		
				
	
	}