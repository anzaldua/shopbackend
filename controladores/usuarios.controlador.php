<?php 

	class ControladorUsuarios{

		/*============================================
		=            Mostrar Total Usuarios            =
		============================================*/
		
		static public function ctrMostrarTotalUsuarios($orden){

			$tabla = "usuarios";

			$respuesta = ModeloUsuarios::mdlMostrarTotalUsuarios($tabla, $orden);

			return $respuesta;

		}
		
		/*=====  End of Mostrar Total Usuarios  ======*/
				

	}