<?php 

	class ControladorVisitas{

		/*============================================
		=            Mostrar Total Visitas            =
		============================================*/
		
		public function ctrMostrarTotalVisitas(){

			$tabla = "visitapaises";

			$respuesta = ModeloVisitas::mdlMostrarTotalVisitas($tabla);

			return $respuesta;

		}
		
		/*=====  End of Mostrar Total Visitas  ======*/

		/*================================================
		=            Mostrar Paises de visita            =
		================================================*/
		
		static public function ctrMostrarPaises($orden){

			$tabla = "visitapaises";

			$respuesta = ModeloVisitas::mdlMostrarPaises($tabla, $orden);

			return $respuesta;

		}
		
		/*=====  End of Mostrar Paises de visita  ======*/
		
				

	}