<!-- Notificaciones -->

<li class="dropdown notifications-menu">

	<!-- DropDown Toggle -->
	<a href="#" class="dropdown-toggle" data-toggle="dropdown">

      <i class="fa fa-bell-o"></i>

      <span class="label label-warning">10</span>

    </a>

    <!-- Dropdown Menu -->

     <ul class="dropdown-menu">

		<li class="header">Tienes 3 Notificaciones</li>

		<li>

           <!-- Menu -->
            <ul class="menu">
              
              <li>
				<!-- Usuarios -->
				<a href="usuarios">

                      <i class="fa fa-users text-aqua"></i> 5 Nuevos usuarios se han registrado hoy!!

                </a>

              </li>

              <li>
				<!-- Ventas -->
				<a href="usuarios">

                      <i class="fa fa-shopping-cart text-aqua"></i> 3 Nuevas ventas hoy!!
                      
                </a>

              </li>

              <li>
				<!-- Visitas -->
				<a href="usuarios">

                      <i class="fa fa-map-marker text-aqua"></i> 55 Nuevas visitas hoy!!
                      
                </a>

              </li>

          	</ul>

      	</li>
	
 	 </ul>
	
</li>