<!-- Usuarios -->

<!-- User Menu -->

<li class="dropdown user user-menu">
	
	<!-- Dropdown Toggle -->
	<a href="#" class="dropdown-toggle" data-toggle="dropdown">

      <img src="vistas/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">

      <span class="hidden-xs">Alex Rye</span>

    </a>

    <!-- Dropdown Menu -->

    <ul class="dropdown-menu">

		<li class="user-header">

	        <img src="vistas/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

	        <p>

	          Alex Rye

	        </p>

	    </li>	

		<!-- Footer del Usuario -->

		 <li class="user-footer">

	        <div class="pull-left">

	          <a href="perfil" class="btn btn-default btn-flat">Perfil</a>

	        </div>

	        <div class="pull-right">

	          <a href="salir" class="btn btn-default btn-flat">Salir</a>

	        </div>

	     </li>

	</ul>

</li>