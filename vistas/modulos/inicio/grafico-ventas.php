<?php 

	error_reporting(0);

	$respuesta = ControladorVentas::ctrMostrarVentas();

	$arrayFechas = array();
	$arrayFechaPago = array();

	foreach ($respuesta as $key => $value) {

		/* Grafico en linea */
		if($value["metodo"] != "gratis"){

			/* Capturamos solo el ano y mes */
			$fecha = substr($value["fecha"], 0, 7);

			/* Capturamos las fechas en un array */
			array_push($arrayFechas, $fecha);

			/* Capturamos fechas y pagos en el mismo array */
			$arrayFechaPago = array($fecha => $value["pago"]);

			/* Sumamos pagos que hayan ocurrido el mismo mes */
			foreach ($arrayFechaPago as $key => $value) {

				$sumaPagosMes[$key] += $value;
			}

		}

	}

	/* Evitar repetir fechas */
	$noRepetirFechas = array_unique($arrayFechas);

 ?>


<!-- Grafico de Ventas -->
<div class="box box-solid bg-teal-gradient">
	
		<div class="box-header">
			
			<i class="fa fa-th"></i>

			<h3 class="box-title">Grafico de Ventas</h3>

			<div class="box-tools pull-right">
				
				<button type="button" class="btn bg-teal btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>

			</div>

		</div>

	<!-- Box Body -->

	<div class="box-body border-radius-none">
		
		<div class="chart" id="line-chart" style="height: 250px;"></div>

	</div>

</div>

<script>
	
	var line = new Morris.Line({
    element          : 'line-chart',
    resize           : true,
    data             : [

    <?php  

    foreach ($noRepetirFechas as $value) {
    	
    	echo "{ y: '".$value."', ventas: ".$sumaPagosMes[$value]." },";

    }

      echo "{ y: '".$value."', ventas: ".$sumaPagosMes[$value]." }";

    ?>

    ],
    xkey             : 'y',
    ykeys            : ['ventas'],
    labels           : ['Ventas'],
    lineColors       : ['#e9e9e9'],
    lineWidth        : 2,
    hideHover        : 'auto',
    gridTextColor    : '#fff',
    gridStrokeWidth  : 0.4,
    pointSize        : 4,
    pointStrokeColors: ['#efefef'],
    gridLineColor    : '#efefef',
    gridTextFamily   : 'Open Sans',
    preUnits         : "$",
    gridTextSize     : 10
  });

</script>

