<!-- Ultimos Usuarios -->

<!-- User List -->
<div class="box box-danger">
	
	<!-- box header -->
	<div class="box-header with-border">
		
		<h3 class="box-title">Ultimos Usuarios Registrados</h3>

		<div class="box-tools pull-right">
			
			<button type="button" class="btn btn-box-tools" data-widget="collapse"><i class="fa fa-minus"></i></button>

		</div>

	</div>
	<!-- /box header -->

	<!-- Box Body -->

	<div class="box-body no-padding">
		
		<!-- Users List -->
		<ul class="users-list clearfix">
			
			<li>
				
				<img src="vistas/dist/img/user1-128x128.jpg" alt="User Image">
				<a href="#" class="users-list-name">Pumita</a>
				<span class="users-list-date">Today</span>

			</li>

			<li>
				
				<img src="vistas/dist/img/user8-128x128.jpg" alt="User Image">
				<a href="#" class="users-list-name">Alex Rye</a>
				<span class="users-list-date">Yesterday</span>

			</li>

			<li>
				
				<img src="vistas/dist/img/user7-128x128.jpg" alt="User Image">
				<a href="#" class="users-list-name">Betoxx</a>
				<span class="users-list-date">12 Jan</span>

			</li>

			<li>
				
				<img src="vistas/dist/img/user6-128x128.jpg" alt="User Image">
				<a href="#" class="users-list-name">Cooper</a>
				<span class="users-list-date">11 March</span>

			</li>

			<li>
				
				<img src="vistas/dist/img/user3-128x128.jpg" alt="User Image">
				<a href="#" class="users-list-name">Hamz</a>
				<span class="users-list-date">Never</span>

			</li>

		</ul>
		<!-- /Users List -->

	</div>
	<!-- /Box Body -->

	<!-- Box Footer -->
	<div class="box-footer text-center">
		
		<a href="usuarios" class="text-uppercase">Ver todos los usuarios</a>

	</div>
	<!-- /Box Footer -->
</div>
<!--/ User List -->