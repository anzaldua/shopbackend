<?php 

  $ventas = ControladorVentas::ctrMostrarTotalVentas();

  $visitas = ControladorVisitas::ctrMostrarTotalVisitas();

  $usuarios = ControladorUsuarios::ctrMostrarTotalUsuarios("id");

  $totalUsuarios = count($usuarios);

  $productos = ControladorProductos::ctrMostrarTotalProductos("id");

  $totalProductos = count($productos);

 ?>

<!-- Small boxes (Stat box) -->
<div class="col-lg-3 col-xs-6">

  <!-- small box -->
  <div class="small-box bg-aqua">

    <div class="inner">

      <h3>$ <?php echo number_format($ventas["total"]); ?></h3>

      <p>Ventas</p>

    </div>

    <div class="icon">

      <i class="ion ion-bag"></i>

    </div>

    <a href="ventas" class="small-box-footer">Mas Info <i class="fa fa-arrow-circle-right"></i></a>

  </div>

</div>

<!-- ./col -->
<div class="col-lg-3 col-xs-6">

  <!-- small box -->
  <div class="small-box bg-green">

    <div class="inner">

      <h3><?php echo $visitas["total"]; ?></sup></h3>

      <p>Visitas</p>

    </div>

    <div class="icon">

      <i class="ion ion-stats-bars"></i>

    </div>

    <a href="visitas" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>

  </div>

</div>

<!-- ./col -->
<div class="col-lg-3 col-xs-6">

  <!-- small box -->
  <div class="small-box bg-yellow">

    <div class="inner">

      <h3><?php echo $totalUsuarios; ?></h3>

      <p>Usuarios Registrados</p>

    </div>

    <div class="icon">

      <i class="ion ion-person-add"></i>

    </div>

    <a href="usuarios" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>

  </div>

</div>

<!-- ./col -->
<div class="col-lg-3 col-xs-6">

  <!-- small box -->
  <div class="small-box bg-red">

    <div class="inner">

      <h3><?php echo number_format($totalProductos); ?></h3>

      <p>Productos</p>

    </div>

    <div class="icon">

      <i class="ion ion-pie-graph"></i>

    </div>

    <a href="productos" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>

  </div>
  
</div>
