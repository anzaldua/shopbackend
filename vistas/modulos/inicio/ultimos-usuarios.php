<?php 

	$usuarios = ControladorUsuarios::ctrMostrarTotalUsuarios("fecha");

	$url = Ruta::ctrRuta();

 ?>

<!-- Ultimos Usuarios -->

<!-- User List -->
<div class="box box-danger">
	
	<!-- box header -->
	<div class="box-header with-border">
		
		<h3 class="box-title">Ultimos Usuarios Registrados</h3>

		<div class="box-tools pull-right">
			
			<button type="button" class="btn btn-box-tools" data-widget="collapse"><i class="fa fa-minus"></i></button>

		</div>

	</div>
	<!-- /box header -->

	<!-- Box Body -->

	<div class="box-body no-padding">
		
		<!-- Users List -->
		<ul class="users-list clearfix">

			<?php 

			if(count($usuarios) > 8){

				$totalUsuarios = 8;

			}else{

				$totalUsuarios = count($usuarios);

			}

				for ($i=0; $i < $totalUsuarios; $i++) { 

					if($usuarios[$i]["foto"] != ""){

						if($usuarios[$i]["modo"] != "directo"){

							echo '<li>
					
								<img src="'.$usuarios[$i]["foto"].'" alt="User Image" style="width:70%">
								<p class="users-list-name">'.$usuarios[$i]["nombre"].'</p>
								<span class="users-list-date">'.substr($usuarios[$i]["fecha"], 0, 10).'</span>

							</li>';

						}else{

							echo '<li>
					
								<img src="'.$url.$usuarios[$i]["foto"].'" alt="User Image" style="width:70%">
								<p class="users-list-name">'.$usuarios[$i]["nombre"].'</p>
								<span class="users-list-date">'.substr($usuarios[$i]["fecha"], 0, 10).'</span>

							</li>';

						}

						
						
					}else{

						echo '<li>
					
								<img src="vistas/img/usuarios/default/anonymous.png" alt="User Image" style="width:70%">
								<p class="users-list-name">'.$usuarios[$i]["nombre"].'</p>
								<span class="users-list-date">'.substr($usuarios[$i]["fecha"], 0, 10).'</span>

							</li>';

					}
					
					

				}

			 ?>

		</ul>
		<!-- /Users List -->

	</div>
	<!-- /Box Body -->

	<!-- Box Footer -->
	<div class="box-footer text-center">
		
		<a href="usuarios" class="text-uppercase">Ver todos los usuarios</a>

	</div>
	<!-- /Box Footer -->
</div>
<!--/ User List -->