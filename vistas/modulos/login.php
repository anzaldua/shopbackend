<div class="login-box">

  <div class="login-logo">

    <img src="vistas/img/plantilla/logo.png" class="img-responsive" style="padding:10px 50px;">

  </div>

  <!-- Cuerpo del Login -->

  <div class="login-box-body">

    <p class="login-box-msg">Bienvenido de Nuevo</p>

    <form method="post">

      <div class="form-group has-feedback">

        <input type="email" class="form-control" placeholder="Ingrese su Correo Corporativo" name="ingEmail">

        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>

      </div>

      <div class="form-group has-feedback">

        <input type="password" class="form-control" placeholder="Ingrese su Password" name="ingPassword">

        <span class="glyphicon glyphicon-lock form-control-feedback"></span>

      </div>

      <div class="row">

        <div class="col-xs-4">

          <button type="submit" class="btn btn-primary btn-block btn-flat">Ingresar</button>

        </div>

      </div>

      <?php 

        $login = new ControladorAdministradores();
        $login -> ctrIngresoAdministrador();

       ?>

    </form>

  </div>

</div>