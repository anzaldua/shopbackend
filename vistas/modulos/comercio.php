<div class="content-wrapper">
  
  <section class="content-header">
    
      <h1>Gestor Comercio </h1>

      <ol class="breadcrumb">
        
        <li><a href="inicio"><i class="fa fa-dashboard"></i>Inicio</a></li>

        <li class="active">Gestor Comercio</li>

      </ol>

  </section>

  <section class="content">
    
    <div class="row">
      
      <div class="col-md-6">
        
        <!--=============================
        =            Bloque1            =
        ==============================-->
        
        <?php 

          /*===========================================================
          =            Administracion del Logotipo e Icono            =
          ===========================================================*/
          
            include "comercio/logotipo.php";
          
            
          

         ?> 
        
        <!--====  End of Bloque1  ====-->
        

      </div>

      <div class="col-md-6">
        
        <!--=============================
        =            Bloque2            =
        ==============================-->
        
        <h1>Bloque 2</h1>
        
        <!--====  End of Bloque2  ====-->

      </div>

    </div>

  </section>

</div>