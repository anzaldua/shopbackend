/*==================================
=            Subir Logo            =
==================================*/

$("#subirLogo").change(function(){

	var imagenLogo = this.files[0];

	/* Validamos el formato de la imagen sea JPG o PNG */

	if(imagenLogo["type"] != "image/jpeg" && imagenLogo["type"] != "image/png"){

		$("#subirLogo").val("");

		swal({

			title: "Error al subir la imagen",
			text: "La imagen debe estar en formato JPG o PNG",
			type: "error",
			confirmButtonText: "Cerrar"
		});

	/* Validamos el peso de la imagen */

	}else if(imagenLogo["size"] > 2000000){

		$("#subirLogo").val("");

		swal({

			title: "Error al subir la imagen",
			text: "La imagen no debe pesar mas de 2MB",
			type: "error",
			confirmButtonText: "Cerrar"
		});

	/* Previsualizamos la imagen */
	}else{

  		var datosImagen = new FileReader;
  		datosImagen.readAsDataURL(imagenLogo);

  		$(datosImagen).on("load", function(event){

  			var rutaImagen = event.target.result;

  			$(".previsualizarLogo").attr("src", rutaImagen);

  		})

  	}

	/* Guardar el Logotipo */	

	$("#guardarLogo").click(function(){

  		var datos = new FormData();
  		datos.append("imagenLogo", imagenLogo);

  		$.ajax({

			url:"ajax/comercio.ajax.php",
			method: "POST",
			data: datos,
			cache: false,
			contentType: false,
			processData: false,
			success: function(respuesta){
				
				console.log("respuesta", respuesta);

			}

		})

	})
	
})

/*=====  End of Subir Logo  ======*/
