<?php 

	require_once "conexion.php";

	class ModeloComercio{

		/*=====================================================================
		=            Muestra todo lo que se encuentra en plantilla            =
		=====================================================================*/

		static public function mdlSeleccionarPlantilla($tabla){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");

			$stmt ->execute();

			return $stmt -> fetch();

			$stmt -> close();

			$stmt = null; 

		}

		/*=====  End of Muestra todo lo que se encuentra en plantilla  ======*/

		/*===============================================
		=            Actualizar Logo o Icono            =
		===============================================*/
		
		static public function mdlActualizarLogoIcono($tabla, $id, $item, $valor){

			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET $item = :$item WHERE id = :id");

			$stmt->bindParam(":".$item, $valor, PDO::PARAM_STR);
			$stmt->bindParam(":id", $id, PDO::PARAM_INT);

			if($stmt->execute()){

				return "ok";

			}else{

				return "error";
			
			}

			$stmt->close();
			$stmt = null;

		}
		
		/*=====  End of Actualizar Logo o Icono  ======*/
	
	}