<?php 

	require_once "conexion.php";

	class ModeloVentas{

		/*==================================================
		=            Mostrar el Total de Ventas            =
		==================================================*/
		
		static public function mdlMostrarTotalVentas($tabla){

			$stmt = Conexion::conectar()->prepare("SELECT SUM(pago) as total FROM $tabla");

			$stmt -> execute();

			return $stmt ->fetch();

			$stmt -> close();

			$stmt = null;

		}
		
		/*=====  End of Mostrar el Total de Ventas  ======*/


		/*======================================
		=            Mostrar Ventas            =
		======================================*/
		
		static public function mdlMostrarVentas($tabla){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");

			$stmt -> execute();

			return $stmt ->fetchAll();

			$stmt -> close();

			$stmt = null;

		}
		
		/*=====  End of Mostrar Ventas  ======*/
		
		

	}