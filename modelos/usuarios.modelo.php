<?php 

	require_once "conexion.php";

	class ModeloUsuarios{

		/*==================================================
		=            Mostrar el Total de Usuarios            =
		==================================================*/
		
		static public function mdlMostrarTotalUsuarios($tabla, $orden){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla ORDER BY $orden DESC");

			$stmt -> execute();

			return $stmt ->fetchAll();

			$stmt -> close();

			$stmt = null;

		}
		
		/*=====  End of Mostrar el Total de Usuarios  ======*/
		

	}