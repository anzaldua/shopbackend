<?php 

	require_once "conexion.php";

	class ModeloProductos{

		/*==================================================
		=            Mostrar el Total de Productos            =
		==================================================*/
		
		static public function mdlMostrarTotalProductos($tabla, $orden){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla ORDER BY $orden DESC");

			$stmt -> execute();

			return $stmt ->fetchAll();

			$stmt -> close();

			$stmt = null;

		}
		
		/*=====  End of Mostrar el Total de Productos  ======*/

		/*===========================================
		=            Mostrar Suma Ventas            =
		===========================================*/
		
		static public function mdlMostrarSumaVentas($tabla){

			$stmt = Conexion::conectar()->prepare("SELECT SUM(ventas) as total FROM $tabla" );

			$stmt -> execute();

			return $stmt ->fetch();

			$stmt -> close();

			$stmt = null;

		}
		
		/*=====  End of Mostrar Suma Ventas  ======*/
		
	}
		
