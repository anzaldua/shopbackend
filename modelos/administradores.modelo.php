<?php 

	require_once "conexion.php";


	class ModeloAdministradores{

		/*===============================================
		=            Mostrar Administradores            =
		===============================================*/
		
		static public function mdlMostrarAdministradores($tabla, $item, $valor){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");

			$stmt ->bindParam(":".$item, $valor, PDO::PARAM_INT);

			$stmt -> execute();

			return $stmt -> fetch();

			$stmt -> close();

			$stmt = null;

		}
		/*=====  End of Mostrar Administradores  ======*/
	}