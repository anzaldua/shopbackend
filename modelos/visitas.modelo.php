<?php 

	require_once "conexion.php";

	class ModeloVisitas{

		/*==================================================
		=            Mostrar el Total de Visitas            =
		==================================================*/
		
		static public function mdlMostrarTotalVisitas($tabla){

			$stmt = Conexion::conectar()->prepare("SELECT SUM(cantidad) as total FROM $tabla");

			$stmt -> execute();

			return $stmt ->fetch();

			$stmt -> close();

			$stmt = null;

		}
		
		/*=====  End of Mostrar el Total de Visitas  ======*/

		/*======================================
		=            Mostrar Paises            =
		======================================*/
		
		static public function mdlMostrarPaises($tabla, $orden){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla ORDER BY $orden DESC");

			$stmt -> execute();

			return $stmt ->fetchAll();

			$stmt -> close();

		}
		
		
		/*=====  End of Mostrar Paises  ======*/
		
		

	}