<?php 
	
	require_once "../controladores/comercio.controlador.php";
	require_once "../modelos/comercio.modelo.php";

	class AjaxComercio{

		/*========================================
		=            Cambiar Logotipo            =
		========================================*/
		
		public $imagenLogo;

		public function ajaxCambiarLogotipo(){

			$item = "logo";
			$valor = $this->imagenLogo;

			$respuesta = ControladorComercio::ctrActualizarLogoIcono($item, $valor);

			echo $respuesta;


		}
		
		/*=====  End of Cambiar Logotipo  ======*/

	}

	/*========================================
	=            Cambiar Logotipo            =
	========================================*/
	
	if(isset($_FILES["imagenLogo"])){

		$logotipo = new AjaxComercio();
		$logotipo -> imagenLogo = $_FILES["imagenLogo"];
		$logotipo -> ajaxCambiarLogotipo();

	}